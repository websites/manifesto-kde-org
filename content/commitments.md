---
layout: other
menu:
  main:
    weight: 3
    name: Commitments
title: Commitments of a KDE Project
other: benefits
---
The KDE Project designation carries with it certain commitments:

* Respect the [KDE Code of Conduct](https://kde.org/code-of-conduct/)
* Support the [KDE Vision](https://community.kde.org/KDE/Vision) and [KDE Mission](https://community.kde.org/KDE/Mission)
* There is no mandatory Contributor License Agreement
* Technical requirements
  * The project stays true to [established practices](https://community.kde.org/Policies) common to similar KDE projects
  * All source materials are hosted on infrastructure available to and writable by all KDE contributor accounts
  * Online services associated with the project are either hosted on KDE infrastructure or have an action plan that ensures continuity which is approved by the KDE system administration team
* Copyrights, trademarks and patents
  * [KDE licensing policy](https://community.kde.org/Policies/Licensing_Policy) is respected
  * KDE branding guidelines are respected
  * If the authors of the software abandon it or disappear, they agree to transfer the trademark to the next maintainer
  * If the code is covered by patents registered by the project itself, those patents must be licensed freely
