---
title: The KDE Manifesto
---
<div class="text">We are a community of technologists, designers, writers and advocates who work to ensure freedom for all people through our software. Because of this work we have come to value:</div>

<span class="value">Open Governance</span> to ensure engagement in our leadership and decision processes;

<span class="value">Free Software</span> to ensure the result of our work is available to all people for all time;

<span class="value">Inclusivity</span> to ensure that all people are welcome to join us and participate;

<span class="value">Innovation</span> to ensure that new ideas constantly emerge to better serve people;

<span class="value">Common Ownership</span> to ensure that we stay united;

<span class="value">End-User Focus</span> to ensure our work is useful to all people.

<div class="text">That is, in pursuit of our goal, we have found these items essential to define and stay true to ourselves.</div>

<div class="addendum">From those values we derived the <a href="../benefits">benefits</a> and <a href="../commitments">commitments</a> of a KDE Project.</div>
