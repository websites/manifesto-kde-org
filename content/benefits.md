---
layout: other
menu:
  main:
    weight: 2
    name: Benefits
title: Benefits of a KDE Project
other: commitments
---
Being part of the international KDE community conveys certain benefits:

* To stand on the shoulders of giants
  * Make use of KDE infrastructure for project hosting
  * Benefit from the experience of the KDE sysadmins
  * Get support from the larger community with development, documentation, translation, testing, bug handling, etc.
  * Use opportunities to integrate with a large ecosystem of end-user products
  * Interaction with teams that have common values, leading to the cross-pollination of ideas and innovations
* Enjoy representation and support by KDE e.V.
  * Participate in Akademy and other KDE events
  * Receive financial and organizational support
  * Know that your trademarks can be secured
  * Know that your licensing wishes can be protected via the [Fiduciary Licensing Agreement](https://ev.kde.org/rules/fla/)
* Increase your visibility through the reputation of the KDE community and KDE promotion tools such as:
  * publishing project updates at [KDE Blogs](https://blogs.kde.org/) and employing other KDE promotion channels
  * having your project reachable through a subdomain under kde.org
  * having a \#kde-<projectname\> IRC channel on the Libera Chat network
  * using KDE as an umbrella brand to associate with the KDE community for communication on web sites and other channels
