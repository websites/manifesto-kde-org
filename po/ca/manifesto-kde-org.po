# Translation of manifesto-kde-org.po to Catalan
# Copyright (C) 2022-2025 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# SPDX-FileCopyrightText: 2022, 2023, 2024, 2025 Josep M. Ferrer <txemaq@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: websites-manifesto-kde-org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-09-28 01:50+0000\n"
"PO-Revision-Date: 2025-01-28 14:54+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: config.yaml:0
msgid "KDE Manifesto"
msgstr "Manifest de KDE"

#: config.yaml:0
msgid ""
"We are a community of technologists, designers, writers and advocates who "
"work to ensure freedom for all people through our software. These are our "
"values and principles."
msgstr ""
"Som una comunitat de tecnòlegs, dissenyadors, escriptors i defensors que "
"treballen per garantir la llibertat per a totes les persones a través del "
"nostre programari. Aquests són els nostres valors i principis."

#: content/_index.md:0
msgid "The KDE Manifesto"
msgstr "El manifest de KDE"

#: content/_index.md:4
msgid ""
"<div class=\"text\">We are a community of technologists, designers, writers "
"and advocates who work to ensure freedom for all people through our "
"software. Because of this work we have come to value:</div>\n"
msgstr ""
"<div class=\"text\">Som una comunitat de tecnòlegs, dissenyadors, escriptors "
"i defensors que treballen per garantir la llibertat per a totes les persones "
"a través del nostre programari. Gràcies a aquest treball hem arribat a uns "
"valors:</div>\n"

#: content/_index.md:6
msgid ""
"<span class=\"value\">Open Governance</span> to ensure engagement in our "
"leadership and decision processes;"
msgstr ""
"<span class=\"value\">Governança oberta</span> per a assegurar el compromís "
"en els nostres processos de lideratge i decisió;"

#: content/_index.md:8
msgid ""
"<span class=\"value\">Free Software</span> to ensure the result of our work "
"is available to all people for all time;"
msgstr ""
"<span class=\"value\">Programari lliure</span> per a assegurar que el "
"resultat del nostre treball està disponible per a totes les persones durant "
"tot el temps;"

#: content/_index.md:10
msgid ""
"<span class=\"value\">Inclusivity</span> to ensure that all people are "
"welcome to join us and participate;"
msgstr ""
"<span class=\"value\">Inclusivitat</span> per a assegurar que totes les "
"persones són benvingudes a unir-se a nosaltres i participar;"

#: content/_index.md:12
msgid ""
"<span class=\"value\">Innovation</span> to ensure that new ideas constantly "
"emerge to better serve people;"
msgstr ""
"<span class=\"value\">Innovació</span> per a assegurar que les noves idees "
"sorgeixen constantment per a servir millor a les persones;"

#: content/_index.md:14
msgid ""
"<span class=\"value\">Common Ownership</span> to ensure that we stay united;"
msgstr ""
"<span class=\"value\">Propietat comuna</span> per a assegurar que ens "
"mantinguem units;"

#: content/_index.md:16
msgid ""
"<span class=\"value\">End-User Focus</span> to ensure our work is useful to "
"all people."
msgstr ""
"<span class=\"value\">Centre en l'usuari final</span> per a assegurar que el "
"nostre treball és útil a totes les persones."

#: content/_index.md:18
msgid ""
"<div class=\"text\">That is, in pursuit of our goal, we have found these "
"items essential to define and stay true to ourselves.</div>\n"
msgstr ""
"<div class=\"text\">És a dir, a la recerca del nostre objectiu, hem trobat "
"aquests elements essencials que ens defineixen i ens mantenen fidels a "
"nosaltres mateixos.</div>\n"

#: content/_index.md:20
msgid ""
"<div class=\"addendum\">From those values we derived the <a href=\"../"
"benefits\">benefits</a> and <a href=\"../commitments\">commitments</a> of a "
"KDE Project.</div>\n"
msgstr ""
"<div class=\"addendum\">D'aquests valors hem obtingut els <a href=\"../"
"benefits\">beneficis</a> i els <a href=\"../commitments\">compromisos</a> "
"d'un projecte de KDE.</div>\n"

#: content/benefits.md:0 i18n/en.yaml:0
msgid "Benefits of a KDE Project"
msgstr "Beneficis d'un projecte KDE"

#: content/benefits.md:0
msgid "Benefits"
msgstr "Beneficis"

#: content/benefits.md:10
msgid "Being part of the international KDE community conveys certain benefits:"
msgstr "Ser part de la comunitat internacional de KDE aporta certs beneficis:"

#: content/benefits.md:12
msgid "To stand on the shoulders of giants"
msgstr "Pujar a l'espatlla de gegants"

#: content/benefits.md:13
msgid "Make use of KDE infrastructure for project hosting"
msgstr "Fer ús de la infraestructura de KDE per a l'allotjament de projectes"

#: content/benefits.md:14
msgid "Benefit from the experience of the KDE sysadmins"
msgstr "Beneficiar-se de l'experiència dels administradors de sistemes de KDE"

#: content/benefits.md:15
msgid ""
"Get support from the larger community with development, documentation, "
"translation, testing, bug handling, etc."
msgstr ""
"Obtenir suport d'una comunitat més gran amb el desenvolupament, la "
"documentació, la traducció, les proves, la gestió d'errors, etc."

#: content/benefits.md:16
msgid ""
"Use opportunities to integrate with a large ecosystem of end-user products"
msgstr ""
"Utilitzar oportunitats per a integrar-se en un gran ecosistema de productes "
"d'usuari final"

#: content/benefits.md:17
msgid ""
"Interaction with teams that have common values, leading to the cross-"
"pollination of ideas and innovations"
msgstr ""
"Interacció amb equips que tenen valors comuns, que condueixen a la "
"pol·linització creuada d'idees i innovacions"

#: content/benefits.md:18
msgid "Enjoy representation and support by KDE e.V."
msgstr "Gaudir de la representació i el suport de la KDE e.V."

#: content/benefits.md:19
msgid "Participate in Akademy and other KDE events"
msgstr "Participar en l'Akademy i altres esdeveniments de KDE"

#: content/benefits.md:20
msgid "Receive financial and organizational support"
msgstr "Rebre suport financer i organitzatiu"

#: content/benefits.md:21
msgid "Know that your trademarks can be secured"
msgstr "Saber que les vostres marques comercials es poden assegurar"

#: content/benefits.md:22
msgid ""
"Know that your licensing wishes can be protected via the [Fiduciary "
"Licensing Agreement](https://ev.kde.org/rules/fla/)"
msgstr ""
"Saber que la vostra voluntat de llicència es pot protegir a través de "
"l'[Acord Fiduciari de Llicència](https://ev.kde.org/rules/fla/)"

#: content/benefits.md:23
msgid ""
"Increase your visibility through the reputation of the KDE community and KDE "
"promotion tools such as:"
msgstr ""
"Augmentar la vostra visibilitat a través de la reputació de la comunitat KDE "
"i les eines de promoció de KDE com:"

#: content/benefits.md:24
msgid ""
"publishing project updates at [KDE Blogs](https://blogs.kde.org/) and "
"employing other KDE promotion channels"
msgstr ""
"publicar actualitzacions de projectes a [KDE Blogs](https://blogs.kde.org/) "
"i emprar altres canals de promoció de KDE"

#: content/benefits.md:25
msgid "having your project reachable through a subdomain under kde.org"
msgstr ""
"tenir el vostre projecte accessible a través d'un subdomini sota kde.org"

#: content/benefits.md:26
msgid "having a \\#kde-<projectname\\> IRC channel on the Libera Chat network"
msgstr "tenint un canal d'IRC \\#kde-<projectname\\> a la xarxa Libera Chat"

#: content/benefits.md:27
msgid ""
"using KDE as an umbrella brand to associate with the KDE community for "
"communication on web sites and other channels"
msgstr ""
"utilitzar KDE com a marca paraigua per a associar-se amb la comunitat KDE "
"per a la comunicació en llocs web i altres canals"

#: content/commitments.md:0 i18n/en.yaml:0
msgid "Commitments of a KDE Project"
msgstr "Compromisos d'un projecte KDE"

#: content/commitments.md:0
msgid "Commitments"
msgstr "Compromisos"

#: content/commitments.md:10
msgid "The KDE Project designation carries with it certain commitments:"
msgstr "La designació de projecte KDE comporta alguns compromisos:"

#: content/commitments.md:12
msgid "Respect the [KDE Code of Conduct](https://kde.org/code-of-conduct/)"
msgstr ""
"Respectar el [codi de conducta de KDE](https://kde.org/code-of-conduct/)"

#: content/commitments.md:13
msgid ""
"Support the [KDE Vision](https://community.kde.org/KDE/Vision) and [KDE "
"Mission](https://community.kde.org/KDE/Mission)"
msgstr ""
"Donar suport a la [Visió de KDE](https://community.kde.org/KDE/Vision) i la "
"[Missió de KDE](https://community.kde.org/KDE/Mission)"

#: content/commitments.md:14
msgid "There is no mandatory Contributor License Agreement"
msgstr "No hi ha obligatorietat d'un Acord de llicència de col·laborador"

#: content/commitments.md:15
msgid "Technical requirements"
msgstr "Requisits tècnics"

#: content/commitments.md:16
msgid ""
"The project stays true to [established practices](https://community.kde.org/"
"Policies) common to similar KDE projects"
msgstr ""
"El projecte es manté fidel a les [pràctiques establertes](https://community."
"kde.org/Policies) habituals en projectes similars a KDE"

#: content/commitments.md:17
msgid ""
"All source materials are hosted on infrastructure available to and writable "
"by all KDE contributor accounts"
msgstr ""
"Totes les dades de codi font s'allotgen en la infraestructura disponible i "
"es poden escriure per part de tots els comptes de col·laboradors de KDE"

#: content/commitments.md:18
msgid ""
"Online services associated with the project are either hosted on KDE "
"infrastructure or have an action plan that ensures continuity which is "
"approved by the KDE system administration team"
msgstr ""
"Els serveis en línia associats amb el projecte estan allotjats en la "
"infraestructura de KDE o tenen un pla d'acció que garanteix la continuïtat "
"que aprova l'equip d'administració de sistemes de KDE"

#: content/commitments.md:19
msgid "Copyrights, trademarks and patents"
msgstr "Drets d'autors, marques comercials i patents"

#: content/commitments.md:20
msgid ""
"[KDE licensing policy](https://community.kde.org/Policies/Licensing_Policy) "
"is respected"
msgstr ""
"Respectar la [Política de llicències de KDE](https://community.kde.org/"
"Policies/LicensingPolicy)"

#: content/commitments.md:21
msgid "KDE branding guidelines are respected"
msgstr "Respectar les directrius de marca de KDE"

#: content/commitments.md:22
msgid ""
"If the authors of the software abandon it or disappear, they agree to "
"transfer the trademark to the next maintainer"
msgstr ""
"Si els autors del programari l'abandonen o desapareixen, accepten transferir "
"la marca comercial al mantenidor següent"

#: content/commitments.md:23
msgid ""
"If the code is covered by patents registered by the project itself, those "
"patents must be licensed freely"
msgstr ""
"Si el codi està cobert per patents registrades pel projecte mateix, aquestes "
"patents han de ser llicenciades lliurement"

#: i18n/en.yaml:0
msgid "Manifesto"
msgstr "Manifest"

#: i18n/en.yaml:0
msgid "Back to the manifesto"
msgstr "Torna al manifest"

#: i18n/en.yaml:0
msgid ""
"This manifesto is inspired by the [Agile](http://agilemanifesto.org/) and "
"[Software Craftmanship](http://manifesto.softwarecraftsmanship.org/) "
"manifestos."
msgstr ""
"Aquest manifest s'inspira en els manifestos d'[Agile](http://agilemanifesto."
"org/) i de [Software Craftmanship](http://manifesto.softwaresmanship.org/)."
