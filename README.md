# Manifesto Website

This is the git repository for [manifesto.kde.org](https://manifesto.kde.org), the website for KDE Manifesto.

As a (Hu)Go module, it requires both Hugo and Go to work.

### Development
Read about the KDE Hugo theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/)

### I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n)
